# Multi-regional and multilingual sites

#### Website

Create a Symfony bundle that implements multi-regional and multilingual sites functionality. 

When users visit [international landing page example.com](http://example.com/) the English language used, the same domain appears in all links on the page. 

When users visit [localized landing page](http://example.com/fr)  the French language used, [website with localized domain example.fr](http://example.fr/) appears in all links on the page.

#### Router

Use Router component functionality to complete Website section requirements. Add an ability to use same website URL rules in CLI Commands. All links should be absolute for WEB and CLI and contain a domain according to specified locale.

#### Translator

Use Translator component functionality to complete Website section requirements.
Translations stored by domain and locale:
`Resources/translations/example.en.yml` for `http://example.com`; 
`Resources/translations/example.fr.yml` for `http://example.fr` and `http://example.com/fr`; 
Domain and locale applied automatically for templates, translator usages and others.

#### Other Requirements

- SOLID
- PSR-2
- [Symfony Code Standards](http://symfony.com/doc/current/contributing/code/standards.html)
- All classes and methods should be documented with PHPdoc, containing not only the list of parameters and their types but also a short description of what the intended purpose of this method or class is.

#### Unit test

All classes and methods should be covered by unit tests.

#### Functional test

General flow should be covered with a functional test. **Please do not add controllers, use Router and Translator functionality.**
