<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '12345678');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '#~WPar5j19:ME7:GmrR5CAr^C4iOqI)CbTjzKwvZ AeJf/pKZej.=JF((Qu_[X=~');
define('SECURE_AUTH_KEY',  'C xP:Ms*H,!Y8L+<%0U|XDA[s6v`fjqNF[xsXZ`;*jo<~H=QioLf/Tjt5!FfF>Ar');
define('LOGGED_IN_KEY',    '?L7[^Uv4M7wRG_v#-i-m.zct;4txf;7N@)8(e.B4QR`GAnoS#=YzFtN88ii*6I(a');
define('NONCE_KEY',        'FjBcGhtHYYI{[Lzk<0m!Wo;Hg>@lHQG&zO<IlWuiAO!q~TH!m8MAYpdrk*ES8A_G');
define('AUTH_SALT',        ',.8#>DWLE/Y9v@WN3bN4szO}Y0G|Vh#ZK&jIDVo@m:Y c9`}M1p:+I#w:W_98f@]');
define('SECURE_AUTH_SALT', 'xhlG#h%g-U1oL8d[UcA<jIPl8mhvRHs0bPIPnkxxY*>OAy?)O}PtpN8h;/^PaCFR');
define('LOGGED_IN_SALT',   '8,pM@LqZu{<41RETWFg3K:Q(?8W,Br; SF5{^8%Qbd>p7x.)VsGlYa]<[BEg{5H=');
define('NONCE_SALT',       'eu(VZsIkU0Kbb8zG/@,0{+Zg.{,C^1Y+N|Kp_&Tdh&yTm%D[#?jNme(BDB$]I84o');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
