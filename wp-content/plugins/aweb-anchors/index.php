<?php
/*
Plugin Name: aweb-anchors
Description: plugin for creating list of anchors
Author: bachatero
Plugin URI: https://bitbucket.org/bachatero/aweb
Version: Номер версии плагина, например: 0.01
Author URI: https://ua.linkedin.com/in/vitaliy-godzevich-82b88587
 */
add_action('admin_menu', 'aweb_admin');

function array_to_json($arr)
{
    $rez = '["' . $arr[0] . '"';
    for ($i = 1; $i < count($arr); $i++) {
        $rez = $rez . ',"' . $arr[$i] . '"';
    }
    return $rez . ']';
}

function aweb_admin()
{
    add_dashboard_page('My Plugin Options', 'aweb_anchors', 'manage_options', 'aweb-dashboard-title', 'aweb_anchors');
}

function aweb_anchors()
{
    readfile(plugin_dir_path(__FILE__) . 'dashboard.php');
}

add_action('wp_ajax_my_echo', 'my_echo');
add_action('wp_ajax_aweb_posts', 'aweb_posts');
add_action('wp_ajax_aweb_anchors', 'aweb_anchors_json');
add_action('wp_ajax_insert_anchor', 'insert_anchor');
add_action('wp_ajax_remove_anchor', 'remove_anchor');
add_action('wp_ajax_update_anchor', 'update_anchor');


function my_echo()
{
    $whatever = $_GET['whatever'];
    echo $whatever;
    wp_die(); // this is required to terminate immediately and return a proper response
}

function aweb_posts()
{
    global $wpdb;
    $sql = 'select guid from wp_posts where post_status="publish" and post_type="post";';
    $posts = $wpdb->get_col($sql);
    echo array_to_json($posts);
    wp_die();
}

function aweb_anchors_json()
{
    global $wpdb;
    $sql = 'SELECT * FROM `wp_anchors`;';
    $anchors = $wpdb->get_results($sql);
    echo json_encode($anchors, JSON_PRETTY_PRINT);
    wp_die();
}

function insert_anchor()
{
    global $wpdb;
    $wpdb->insert(
        'wp_anchors',
        array(
            'anchor' => $_POST['anchor'],
            'link' => $_POST['link']
        )
    );

    echo json_encode($wpdb->insert_id, JSON_PRETTY_PRINT);
    wp_die();
}

function remove_anchor()
{
    global $wpdb;
    $sql = 'delete from ' . $wpdb->prefix . 'anchors where id=' . $_GET["id"] . ';';
    $wpdb->query($sql);
    echo json_encode($sql, JSON_PRETTY_PRINT);
    wp_die();
}

function update_anchor()
{
    global $wpdb;
    $sql = 'update ' . $wpdb->prefix . 'anchors 
    set `anchor` = "' . $_GET["anchor"] . '" where id=' . $_GET["id"] . ';';
    $wpdb->query($sql);
    echo json_encode($sql, JSON_PRETTY_PRINT);
    wp_die();
}


function aweb_install()
{
    global $wpdb;

    $table_name = $wpdb->prefix . "anchors";
    $charset_collate = $wpdb->get_charset_collate();

    $sql = "SET NAMES utf8;
DROP TABLE IF EXISTS `wp_anchors`;
CREATE TABLE `wp_anchors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `anchor` text NOT NULL,
  `link` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;";

    require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
    dbDelta($sql);
}


add_action('wp_footer', 'anchors_widget');
function anchors_widget()
{
    global $wpdb;
    $sql = 'SELECT * FROM `wp_anchors`;';
    $anchors = $wpdb->get_results($sql);
    echo 'here are anchors:<br>';
    echo '<div class="aweb"><ul>';
    foreach ($anchors as $item) {
        echo "<li><a href='$item->link'>" . $item->anchor . "</a></li>";
    }
    echo '</ul></div>';
    echo '<p>powered by aweb 2017,</p>';
}

?>