<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>dashboard</title>
</head>
<body>
<div id="app">
    <div id="add-link">
        <span v-show="!newAnchor">create anchor</span>
        <input type="text" v-model="newAnchor" placeholder="new anchor">
        <select v-show="newAnchor" v-model="newLink">
            <option v-for="link in links">{{link}}</option>
        </select>
        <input v-show="newAnchor && newLink" v-on:click="addLink" value="add anchor" type="button">
    </div>
    <div id="anchors">

        <div v-for="(item,index) in anchors">
            {{index}}
            <input v-on:click="edit(index)" v-show="current!==index" type="button" value="edit">
            <input v-on:click="remove(index)" v-show="current!==index" type="button" value="delete">
            <a v-bind:href="item.link" v-show="current!==index">{{item.anchor}}</a>

            <input v-on:click="save(index)" v-show="current===index" type="button" value="save">
            <input type="edit" v-model="item.anchor" v-show="current===index">
        </div>
    </div>

</div>
</div>

<!--<script src="../wp-content/plugins/aweb-anchors/vue.min.js"></script>-->
<script src="https://unpkg.com/vue/dist/vue.js"></script>
<script>
    var app = new Vue({
        el: '#app',
        data: {
            message: 'Hello Vue!',
            links: [],
            anchors: [],
            newAnchor: '',
            newLink: '',
            newId: null,
            current: null
        },
        methods: {
            addLink: function () {
                console.log('we add ' + this.newAnchor);
                var self = this;
                var request = {
                    action: 'insert_anchor',
                    anchor: this.newAnchor,
                    link: this.newLink
                };
                jQuery.post(ajaxurl, request, function (response) {
                    console.log('Got this from the server: ');
                    response = JSON.parse(response);
                    self.newId = response;
                    console.dir(response);
                    self.anchors.push({
                        id: self.newId,
                        anchor: self.newAnchor,
                        link: self.newLink
                    });
                    self.newAnchor = null;
                    self.newLink = null;
                });
            },
            edit: function (index) {
                console.log('we want to edit ' + index);
                this.current = index;
            },
            save: function (index) {
                var id = this.anchors[index].id;
                console.log('we want to update ' + index);
                var request = {
                    'action': 'update_anchor',
                    'id': id,
                    'anchor': this.anchors[index].anchor
                };
                jQuery.get(ajaxurl, request, function (response) {
                    console.log('Got this: ' + response);
                });
                this.current = null;
            },
            remove: function (index) {
                var id = this.anchors[index].id;
                console.log('we want to delete ' + index);
                var request = {
                    'action': 'remove_anchor',
                    'id': id
                };
                jQuery.get(ajaxurl, request, function (response) {
                    console.log('Got this: ' + response);
                });
                this.anchors.splice(index, 1);
            }
        },
        created: function () {
            console.log('Значение: ' + this.message);
            console.log('jQuery is ' + typeof jQuery);
            var self = this;
            var request = {
                'action': 'aweb_posts'
            };
            jQuery.get(ajaxurl, request, function (response) {
                console.log('Got this from the server: ');
                response = JSON.parse(response);
                self.links = response;
                console.dir(response);
            });

            var request = {
                'action': 'aweb_anchors'
            };
            jQuery.get(ajaxurl, request, function (response) {
                console.log('Got this from the server: ');
                response = JSON.parse(response);
                console.dir(response);
                self.anchors = response;
            });
        }
    })
</script>
</body>
</html>