<div class="clear"></div>
</div>
<footer id="colophon" class="site-footer " role="contentinfo">
  <div class="site-info wrapper">
    <?php
  if (has_nav_menu('footer')) {
					wp_nav_menu(array( 
					'theme_location' => 'footer', 
					'container' => false, 
					'menu_id' => 'footer-nav', 
					'menu_name' => 'footer_nav', 
					'menu_class' => 'footer-nav ', 
					'link_before' => '<span>', 
					'link_after' => '</span>',
					'fallback_cb'=>false,
					'depth'=>1
				));
			}?>
    <p class="site-info centertext footer-copy"> 
    <span class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">&copy; <?php echo date('Y');?> <?php bloginfo( 'name' ); ?></a></span> 
    <span class="site-title"><a href="<?php echo esc_url( 'https://wordpress.org/'); ?>"> <?php printf( __( 'Proudly powered by %s', 'wordstar' ), 'WordPress' ); ?></a></span> 
    <a href="<?php echo esc_url('https://linesh.com/project/wordstar/'); ?>"> <?php _e( 'WordStar', 'wordstar' ); ?></a>,
    <a href="<?php echo esc_url('https://linesh.com/'); ?>"> <?php _e( 'Theme by Linesh Jose', 'wordstar' ); ?></a> 
    </p>
  </div>
</footer>
</div>
<?php wp_footer(); ?>
</body>
</html>