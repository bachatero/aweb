<section class="no-results not-found">
  <header class="page-header"><h1 class="page-title"><?php _e( 'Nothing Found', 'wordstar' ); ?></h1></header>
  <div class="page-content">
    <?php if ( wordstar_is_home_page() && current_user_can( 'publish_posts' ) ) : ?>
    <p><?php echo  __( 'Ready to publish your first post? ','wordstar' ).'<a href="'.esc_url( admin_url( 'post-new.php' ) ).'">'.__('Get started here.', 'wordstar' ).'</a>'; ?></p>
    <?php elseif ( is_search() ) : ?>
    <p> <?php _e( 'Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'wordstar' ); ?></p>
    <div class="search-form-wrap">
      <?php get_search_form(); ?>
    </div>
    <?php else : ?> 
    <p><?php _e( 'It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help.', 'wordstar' ); ?></p>
    <div class="search-form-wrap">
      <?php get_search_form(); ?>
    </div>
    <?php endif; ?>
  </div>
</section>